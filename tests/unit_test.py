from unittest import TestCase, main
from anagrams.anagrams import reverse_words


class AnagramsTest(TestCase):
    def test_count(self):
        self.assertCountEqual(reverse_words('qwerty 12345'), 'ytrewq 12345')

    def test_type(self):
        self.assertTrue(reverse_words('qwerty 12345'), str)

    def test_Equal(self):
        cases = [
            ('qwerty 12345', 'ytrewq 12345'),
            ('12345 QWERTY', '12345 YTREWQ'),
            ('qwerty][)&&&##___ 12345', 'ytrewq][)&&&##___ 12345'),
            ('', '')
        ]
        for text, reverse_text in cases:
            with self.subTest(text):
                self.assertEqual(reverse_words(text), reverse_text)

    def test_NotEqual(self):
        cases = [
            ('qwerty 12345', 'qwerty 12345'),
            ('12345 QWERTY', '12345YTREWQ'),
        ]
        for text, reverse_text in cases:
            with self.subTest(text):
                self.assertNotEqual(reverse_words(text), reverse_text)


class AnagramsTest_atypical_behavior(TestCase):
    def test_input_type(self):
        cases = [
            (123456789),
            ({1: "qwerty", 2: 12345, 3: "YTREWQ"}),
            (['qwerty', 12345]),
            (('qwerty', 12345))
        ]
        for case in cases:
            with self.subTest(case):
                with self.assertRaises(TypeError):
                    reverse_words(case)


if __name__ == '__main__':
    main()
